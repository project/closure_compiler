<?php

/**
 * @file
 * Closure Compiler defines file.
 */

define('CLOSURE_COMPILER_JS_PATH', 'public://js');

define('CLOSURE_COMPILER_LOCAL', 0);
define('CLOSURE_COMPILER_SEND_CONTENTS', 1);
define('CLOSURE_COMPILER_SEND_URL', 2);

define('CLOSURE_COMPILER_DEFAULT_LIMIT', 5);

define('CLOSURE_COMPILER_SOURCE_START', '/*src:');
define('CLOSURE_COMPILER_SOURCE_END', '*/');
