<?php

/**
 * @file
 * Google Closure Compiler defines file.
 */

define('GOOGLE_CLOSURE_COMPILER_URL', 'http://closure-compiler.appspot.com/compile');
define('GOOGLE_CLOSURE_COMPILER_FILE', 'compiler.jar');

define('GOOGLE_CLOSURE_COMPILER_WHITESPACE', 'WHITESPACE_ONLY');
define('GOOGLE_CLOSURE_COMPILER_SIMPLE', 'SIMPLE_OPTIMIZATIONS');
define('GOOGLE_CLOSURE_COMPILER_ADVANCED', 'ADVANCED_OPTIMIZATIONS');
